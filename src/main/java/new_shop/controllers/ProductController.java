package new_shop.controllers;
import new_shop.model.entity.Product;
import new_shop.model.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.Map; /*import java.util.Collections;*/

@Controller @RequestMapping("catalog/products")

public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @GetMapping("list") /*@RequestMapping(value = "list",method = RequestMethod.GET)*/ ModelAndView list() {
        return new ModelAndView("catalog/products/list",
                Map.of("products", this.productRepository.findAll()), HttpStatus.OK);
        /*return new ModelAndView("catalog/products/list",  Collections.singletonMap("products", this.productRepository.findAll()), HttpStatus.OK);*/

    }
    private static Product product;
    static {
        product = new Product(); product.setId(1); product.setName("product_1"); }
    @GetMapping("/{productId}")
    ModelAndView editProduct(@PathVariable String productId)
    { ModelAndView modelAndView = new ModelAndView("catalog/products/product");
        modelAndView.addObject("product", product); return modelAndView;
    }
}
